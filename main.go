package main

import (
	"bufio"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"time"
)

const name = "Vinicius Santos" //Your name
const version = 7.0            //Software version (seven point oh!) Toxicity :)

const monitoringNumber = 15                    //How many times will trigger the monitoring event
const monitoringDelay = 10                     //Delay in seconds between monitorings
const logDelay = 1                             //Delay to show logs
const siteFileName = "sites-to-monitoring.txt" //File name with the sites that you want to monitoring
const logFileName = "logs.log"                 //File name with the logs

var dateFormat = time.Now().Format("02/01/2006 15:04:05") //Date format to dd/MM/yyyy hh:MM:ss

func main() {
	fmt.Println("User:", name, "\nSoftware version:", version)
	for {

		showActions()

		action := getAction()

		switch action {
		case 1:
			fmt.Println("")
			startMonitoring()
		case 2:
			fmt.Println("")
			showLog()
		case 0:
			fmt.Println("See you next time :)")
			os.Exit(0)
		default:
			fmt.Println("Action was not recognized")
			os.Exit(-1)
		}
	}

}

func showActions() {
	fmt.Println("1- Start monitoring")
	fmt.Println("2- Show logs")
	fmt.Println("0- Exit")
}

func getAction() int {
	var command int
	fmt.Print("Choose the action that you want to trigger: ")
	fmt.Scan(&command)

	return command
}

func startMonitoring() {
	fmt.Println("Monitoring")

	sites := mapSitesFile()

	for i := 0; i < monitoringNumber; i++ {

		for _, site := range sites {
			siteMonitoring(site)
		}

		time.Sleep(monitoringDelay * time.Second)
		fmt.Println("")
	}

	fmt.Println("")
}

func siteMonitoring(site string) {
	response, err := http.Get(site)

	if err != nil {
		fmt.Println("Something went wrong!\nError:", err)
	}

	if response.StatusCode == 200 {
		fmt.Println("Site", site, "is online!")
		saveLog(site, true)
	} else {
		fmt.Println("Site", site, "is offline with error", response.StatusCode)
		saveLog(site, false)
	}
}

func mapSitesFile() []string {

	var sites []string

	file, err := os.Open(siteFileName)

	reader := bufio.NewReader(file)

	if err != nil {
		fmt.Println("It was not possible to open the file", siteFileName, "\nError:", err)
	}

	for {
		site, err := reader.ReadString('\n')
		site = strings.TrimSpace(site)

		if err == io.EOF {
			break
		}
		sites = append(sites, site)
	}

	file.Close()

	return sites
}

func saveLog(site string, status bool) {
	file, err := os.OpenFile(logFileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)

	if err != nil {
		fmt.Println("It was not possible to open the file", siteFileName, "\nError:", err)
	}
	if status {
		file.WriteString(dateFormat + " - " + site + " was online!\n")
	} else {
		file.WriteString(dateFormat + " - " + site + " was offline!\n")
	}

	file.Close()

}

func showLog() {

	file, err := ioutil.ReadFile(logFileName)

	if err != nil {
		fmt.Println("It was not possible to open the file", logFileName, "\nError:", err)
	}

	time.Sleep(1 * time.Second)

	fmt.Println(string(file))

	fmt.Println("")

}
